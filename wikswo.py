# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from scipy import special
import matplotlib.pyplot as plt

import numpy as np
from numpy.testing import assert_almost_equal

# <codecell>

# params
a, b, c = 1.,3.5,4. # axon radius
t = [0.5, 2.]
sigma_z, sigma_rho = 1, 1 # conductivities
sigma_i, sigma_e = 0.2, 5
sigma_s = 0.02

# <codecell>

n_comps = 5 #numbers of m
n_vars = 6 #number of coefficient (ABCDEF)
n_t =2 # number of fibers
total_vars = n_vars * n_comps + n_comps* n_t

# <codecell>

dx = 0.1
n_spatial = 2**4
x_space = np.arange(n_spatial, dtype=float) * dx
phi_m = np.exp(-(x_space-x_space.mean())**2/0.1)
plt.plot(x_space, phi_m)

# <codecell>

phi_m_fourier = np.fft.fftshift(np.fft.fft(phi_m))
dk = 2. * np.pi / dx / len(phi_m_fourier) / 2
ks = (np.arange(len(phi_m_fourier)) - len(phi_m_fourier)/2) * dk
plt.plot(ks, np.abs(phi_m_fourier))

# <codecell>

from scipy.special import iv, kn, kv, ivp, kvp

def find_coefficient(k_i):
#k_i = 2# special frequency
    k = ks[k_i]
    if k == 0:
        return np.zeros(total_vars)

    mat_coef = np.zeros((total_vars, total_vars), dtype=np.float64)
    rhs = np.zeros(total_vars, dtype=np.complex128)
    A = mat_coef[:,0::n_vars]
    B = mat_coef[:,1::n_vars]
    C = mat_coef[:,2::n_vars]
    D = mat_coef[:,3::n_vars]
    E = mat_coef[:,4::n_vars]
    F = mat_coef[:,5::n_vars]

    
    # eq. 14

    for m in range(n_comps):
        i_eq = m
        A[i_eq, m] = special.iv(m, np.abs(k) * a)
        a_norm = np.abs(k) * np.sqrt(sigma_z / sigma_rho) * a 
        B[i_eq, m] = -special.iv(m, a_norm)
        C[i_eq, m] = -special.kn(m, a_norm)
    rhs[0] = phi_m_fourier[k_i]

    # eq. 16
    for m in range(n_comps):
        i_eq = m + n_comps
        A[i_eq, m] = sigma_i / np.sqrt(sigma_z * sigma_rho) * special.ivp(m, np.abs(k) * a)
        a_norm = np.abs(k) * np.sqrt(sigma_z / sigma_rho) * a
        B[i_eq, m] = -special.ivp(m, a_norm)
        C[i_eq, m] = -special.kvp(m, a_norm)

    ####

    # eq. 26
    for m in range(n_comps):
        i_eq = m + 2 * n_comps
        D[i_eq, m] = special.iv(m, np.abs(k) * c)
        E[i_eq, m] = special.kn(m, np.abs(k) * c)
        F[i_eq, m] = -special.kn(m, np.abs(k) * c)

    # eq. 27
    for m in range(n_comps):
        i_eq = m + 3 * n_comps
        D[i_eq, m] = special.ivp(m, np.abs(k) * c)
        E[i_eq, m] = special.kvp(m, np.abs(k) * c)
        F[i_eq, m] = -sigma_e / sigma_s * special.kvp(m, np.abs(k) * c)

    ### 

    # eq. 22?
    
        
    b_norm = np.abs(k) * np.sqrt(sigma_z / sigma_rho) * b
    t_norm = np.abs(k) * np.sqrt(sigma_z / sigma_rho) * tt
    for m in range(n_comps):
        i_eq = m + 4 * n_comps
        B[i_eq, 0] = (-1)**m * iv(m, t_norm) * iv(m, b_norm)
        C[i_eq, 0] = iv(m, t_norm) * kn(m, b_norm)

        for n in range(1, n_comps):
            B[i_eq, n] = (-1)**m * (iv(n+m, t_norm) + iv(n-m, t_norm)) * iv(m, b_norm)
            C[i_eq, n] = (iv(n+m, t_norm) + iv(n-m, t_norm)) * kn(m, b_norm)

            D[i_eq, m] = -special.iv(m, np.abs(k) * b)
            E[i_eq, m] = -special.kn(m, np.abs(k) * b)

# eq. 23?
    for m in range(n_comps):
            i_eq = m + 5 * n_comps
            b_norm = np.abs(k) * np.sqrt(sigma_z / sigma_rho) * b
            B[i_eq, 0] = (-1)**m * iv(m, t_norm) * ivp(m, b_norm)
            C[i_eq, 0] = iv(m, t_norm) * kvp(m, b_norm)

            for n in range(1, n_comps):
                B[i_eq, n] = (-1)**m * (iv(n+m, t_norm) + iv(n-m, t_norm)) * ivp(m, b_norm)
                C[i_eq, n] = (iv(n+m, t_norm) + iv(n-m, t_norm)) * kvp(m, b_norm)

                D[i_eq, m] = -sigma_s / np.sqrt(sigma_z * sigma_rho) * special.ivp(m, np.abs(k) * b)
                E[i_eq, m] = -sigma_s / np.sqrt(sigma_z * sigma_rho) * special.kvp(m, np.abs(k) * b)
                        
       
    x = np.linalg.solve(mat_coef, rhs)
    #plt.matshow(1./mat_coef)
    #plt.colorbar()
    #print np.where(mat_coef > 1e20)
    return x

# <codecell>

solutions = np.vstack([find_coefficient(k) for k in range(len(phi_m_fourier))]).T

# <codecell>

Am = solutions[0::n_vars, :]
Bm = solutions[1::n_vars, :]
Cm = solutions[2::n_vars, :]
Dm = solutions[3::n_vars, :]
Em = solutions[4::n_vars, :]
Fm = solutions[5::n_vars, :]

# <codecell>

def calc_potentials_fourier(rho, theta):
    # eq. 9
    A_mkr = Am[:, :, None]
    B_mkr = Bm[:, :, None]
    C_mkr = Cm[:, :, None]
    D_mkr = Dm[:, :, None]
    E_mkr = Em[:, :, None]
    F_mkr = Fm[:, :, None]
    rho = np.array(rho)
    
    # allow negative rho's by shifting theta by pi
    theta = np.array(theta)
    if theta.ndim == 0:
        theta = np.ones(len(rho)) * theta    
    theta[rho < 0] = theta[rho < 0] + np.pi
    rho = np.abs(rho)
    
    k_kr = np.abs(ks[:, None])
    rho_prime = np.sqrt(rho ** 2 + t ** 2 - 2*rho*t*np.cos(theta))
    rho_star_prime = np.sqrt(sigma_z/sigma_rho) * rho_prime
    rho_star = np.sqrt(sigma_z/sigma_rho) * rho
    
    theta_prime = np.arcsin(rho / rho_prime * np.sin(theta))
    t_star = np.sqrt(sigma_z/sigma_rho) * t

    rho_kr = rho[None, :]
    rho_prime_kr = rho_prime[None, :]
    rho_star_prime_kr = rho_star_prime[None, :]
    rho_star_kr = rho_star[None, :]

    k_norm = k_kr * rho_prime_kr
    phi_int = A_mkr[0, :, :]*special.iv(0, k_norm)
    phi_int += 2 * sum([A_mkr[m, :, :]*special.iv(m, k_norm) * np.cos(m*theta_prime) 
                     for m in range(1, n_comps)])
    
    # eq. 10
    k_norm = k_kr * rho_star_prime_kr
    phi_b = B_mkr[0,:,:] * special.iv(0,  k_norm) + C_mkr[0,:,:] * special.kn(0, k_norm)
    phi_b += 2 * sum((B_mkr[m, :, :] * special.iv(m, k_norm) + 
                      C_mkr[m, :, :] * special.kn(m, k_norm)) * np.cos(m * theta_prime) 
                      for m in range(1, n_comps))
    

    # eq. 11
    k_norm = k_kr * rho_kr
    phi_s = D_mkr[0,:,:] * special.iv(0,  k_norm) + E_mkr[0,:,:] * special.kn(0, k_norm)
    phi_s += 2 * sum((D_mkr[m, :, :] * special.iv(m, k_norm) + 
                      E_mkr[m, :, :] * special.kn(m, k_norm)) * np.cos(m * theta) 
                      for m in range(1, n_comps))
    
    # eq. 12
    k_norm = k_kr * rho_kr

    phi_e = F_mkr[0, :, :]*special.kn(0, k_norm)
    phi_e += 2 * sum([F_mkr[m, :, :]*special.kn(m, k_norm) * np.cos(m*theta) 
                     for m in range(1, n_comps)])
    
    phi = np.zeros((len(ks), len(rho)), dtype=np.complex)
    phi[:, rho_prime <= a] = phi_int[:, rho_prime <= a]
    phi[:, (rho_prime > a) & (rho <= b)] = phi_b[:, (rho_prime > a) & (rho <= b)]
    phi[:, (rho > b) & (rho <= c) & (rho_prime > a)] = phi_s[:, (rho > b) & (rho <= c) & (rho_prime > a)] 
    phi[:, rho > c] = phi_e[:, rho > c]
    phi[np.isnan(phi)] = 0
    
    
    if (rho_prime == a).any():
        V_m = np.squeeze(phi_int[:, rho_prime == a] - phi_b[:, rho_prime == a])
        assert_almost_equal(V_m[~np.isnan(V_m)], phi_m_fourier[~np.isnan(V_m)])
    if (rho == b).any():
        assert_almost_equal(phi_b[:, rho == b], 
                            phi_s[:, rho == b])
    if (rho == c).any():
        assert_almost_equal(phi_e[:, rho == c], 
                            phi_s[:, rho == c])
        
    

    return phi

# <codecell>

phi_i = calc_potentials_fourier(np.array([0.5]), np.pi)

# <codecell>

def ifft_potentials(phi):
    return np.fft.ifft(np.fft.fftshift(phi, axes=0), axis=0).real

plt.plot(ifft_potentials(phi_i))
plt.twinx()
plt.plot(phi_m, color='g')

# <codecell>

x = np.linspace(-4, 4, 20)
y=np.linspace(-np.pi,np.pi,20)
phi_i = calc_potentials_fourier(x, 0)
phi_space = ifft_potentials(phi_i)

# <codecell>


# <codecell>

plt.subplot(211)
plt.plot(x, phi_space.T)
plt.xlabel(r'$\rho$')
plt.subplot(212)
plt.plot(phi_space)
plt.xlabel(r'z')
plt.tight_layout()

# <codecell>

plt.matshow(phi_space)
plt.ylabel('z')
plt.xlabel(r'$\rho$')
plt.colorbar()

